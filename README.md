# Creating a Workshop with LiaScript

This repository hosts the source for the workshop `Creating a Workshop with LiaScript`. The content can be viewed using the LiaScript renderer here: [Workshop on LiaScript](https://liascript.github.io/course/?https://api.allorigins.win/raw?url=https://git.rwth-aachen.de/dl/workshops/creating-a-workshop-with-liascript/-/raw/main/lia/workshop.md)

## Structure

Workshop content is stored in the file [./lia/workshop.md](https://git.rwth-aachen.de/dl/workshops/creating-a-workshop-with-liascript/-/blob/main/lia/workshop.md). The [resources](https://git.rwth-aachen.de/dl/workshops/creating-a-workshop-with-liascript/-/tree/main/resources) folder contains images, scripts, and style sheets used by the liascript file.
