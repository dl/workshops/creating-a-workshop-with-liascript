<!--
@author: Jonathan Hartman
@date: March 19th, 2024
@email: hartman@itc.rwth-aachen.de
@repository: https://git.rwth-aachen.de/dl/workshops/creating-a-workshop-with-liascript
version: 0.1.0

link: ../resources/style/custom.css

@important: <b style="color: red"> Important! </b>

@fancy_text: <@2 style="color: @1"> @0 </@2>

@image
<div class="custom-image">
![@0](@1 "@0")
</div>
@end

@check_in
I'll pause here for a moment. Please click the radio button and hit "Submit" when you're ready.

  [(1)] I'm ready to move on!

---

*And if you have time...*

Do you have any questions or comments based on what we just covered?

  [[___]]

How is my pace so far?

  [(Too Slow)(Ok)(A little Fast)]
  [                             ] Pace

@end
-->

# Getting Started...

<h2> HedgeDoc: https://pad.otc.coscine.dev/liascript </h2>

---
The Content in this document is broadly based on the [LiaScript Documentation](https://liascript.github.io/course/?https://raw.githubusercontent.com/liaScript/docs/master/README.md#1)
which provides more detailed looks into all of the items covered here, as well as many additional
topics.

---


*While we're getting situated, take a few moments to respond to the following questions. (Answers*
*are completely anonymous)*

1. **Are you at all familiar with LiaScript?**

   [(X)] Yes
   [(X)] No

2. Do you have an account on a Code Hosting Platform (e.g. GitLab, GitHub, etc.)

   [(X)] Yes
   [(X)] No

3. **How would you describe yourself professionally?**
   (This will generate a word cloud - use `,` to separate phrases)

    [[___]]

4. **What sort of topics might you consider creating a workshop for?**
   (This will generate a word cloud - use `,` to separate phrases)

    [[___]]

5. **How would you respond to the phrase "How familiar are you with ..."**

  [( Not familiar at all)(Not very familiar)(Somewhat familiar)(Very familiar)]
  [                                                                           ] Markdown
  [                                                                           ] HTML
  [                                                                           ] Git
  [                                                                           ] GitLab/GitHub/Bitbucket/etc.
  [                                                                           ] Javascript
  [                                                                           ] Python


# Introduction

Hello! And welcome to our workshop / self-paced interactive textbook. Our purpose is to give you
a general working knowledge of LiaScript and how to create a workshop of your own using this
tool. You might have noticed that this document itself is written in LiaScript (meta!) - that is
not by chance! LiaScript is a simple to write, easy to deploy plain text format we can use to
create tools just like this one, that can be shared with anyone.

By the end of this document, our goal is that you should feel comfortable with the following:

- Writing a LiaScript Workshop yourself
- Sharing a LiaScript document with others
- Adding formatting, sections, and chapters to your workshops
- Embedding other media into your workshop
- Adding interactive elements into your content
- Extending LiaScript to add additional functionality

The idea behind this workshop is that we will spend roughly an hour covering the basics of how
LiaScript works and what it is capable of, before breaking into pairs or individuals to convert or
create our own content.

Our schedule will look something roughly like this:

<!-- data-type="none" -->
| Section                              | Duration           |
| ------------------------------------ | ------------------ |
| Introduction                         | 5 minutes          |
| LiaScript                            | 3 minutes          |
| Sharing / Hosting Content            | 15 minutes         |
| LiaScript Flavored Markdown          | 10 minutes         |
| Quizzes / Checking for Understanding | 15 minutes         |
| Metadata / Macros                    | 15 minutes         |
| Break                                | 5 minutes          |
| Guided Work                          | All Remaining Time |

## Setup

Before we get started, let's make sure we have everything in order so that we can immediately see
the results of our work.

### Hosting Files

The only thing that will be required in order to follow along is a GitHub/GitLab repository that
we can store a file in. The demonstration alongside this document will be be done with a GitLab
account using RWTH's free GitLab Instance. You can alternatively use gitlab.com, github.com,
bitbucket.org.

<div style="text-align: center;">
<h2>Account Creation</h2>
</div>

You will need an account at one one of the following sites. If you already have one, that's great!
If you don't, please take a few moments before the start date to create an account

<div style="width: 75%; margin: auto; display: flex;">
    <a href="https://git.rwth-aachen.de/">
        <img src="https://about.gitlab.com/_nuxt/image/3395f6.svg" alt="GitLab.com" style="width: 200px;">
        <span style="font-size: 3.5rem;">
        @ RWTH
        </span>
    </a>
    <br>
    <a href="https://gitlab.com"><img src="https://about.gitlab.com/_nuxt/image/3395f6.svg" alt="GitLab.com" style="width: 200px;"></a>
    <br>
    <a href="https://github.com">
        <img src="https://github.githubassets.com/assets/GitHub-Mark-ea2971cee799.png" alt="GitHub.com" style="width: 60px;">
        <img src="https://github.githubassets.com/assets/GitHub-Logo-ee398b662d42.png" alt="GitHub.com" style="width: 140px;">
    </a>
</div>

---
**Q. I have another solution!**

Strictly speaking, we only need a url we can host our file at that can be publicly shared. We
would like to stress the ideas of collaborative editing, which is why we are focusing on git
based solutions. You are of course welcome to host your file on whatever service you are
comfortable with.

### Creating a Repository

We'll need a repository set to "public" where we can host our files. I'll demonstrate this using
the RWTH GitLab service, but I'll include a section on GitHub as well.

#### RWTH GitLab

1. From the GitHub Landing page, select the small "+" sign on the top of the left hand navbar.
@image(`New Repository`,../resources/img/setup/rwthgitlab/hosting-gitlab-start-new-repo.png)

2. From the dropdown, select "New Project/Repository"
@image(`New Project / Repository`,../resources/img/setup/rwthgitlab/hosting-gitlab-start-new-repo-2.png)

3. Select "Create blank project" from the provided options
@image(`Create blank project`,../resources/img/setup/rwthgitlab/hosting-gitlab-start-new-repo-3.png)

4. We have a couple things to do on the next page:

    1. Type a project name (it can be whatever you want, but should be concise and descriptive)
    2. Select a namespace for the repository (I put it in my user projects)
    3. @important Make sure the Visibility Level is set to "Public"
    4. Leave the option "Initialize repository with a README" checked.
    5. Click the "Create project" button.
@image(`Configure project`,../resources/img/setup/rwthgitlab/hosting-gitlab-start-new-repo-4.png)

5. Add a blank file to the repository to work in:
@image(`Add a file`,../resources/img/setup/rwthgitlab/hosting-gitlab-start-new-repo-5.png)

6. Set a file name and commit our changes:
@image(`Commit a new file`,../resources/img/setup/rwthgitlab/hosting-gitlab-start-new-repo-6.png)

#### GitLab.com

1. If this is the first time you've made a GitLab account, before you can create a repository you'll have to create a group:
(Make sure to leave the "Include a Getting Started README" option checked.)
@image(`Create a group`,../resources/img/setup/gitlab/hosting-gitlab-start-new-repo.png)

By default, gitlab project groups are created with "Private" visibility. We need ours to be "Public"

2. Click the breadcrumb link at the top of the screen to view the project group:
@image(`Create a group`,../resources/img/setup/gitlab/hosting-gitlab-start-new-repo-2.png)

3. On the left nav bar, go to "Settings" -> "General"
@image(`Navigate to General Settings`,../resources/img/setup/gitlab/hosting-gitlab-start-new-repo-3.png)

4. Set the Group Visibility to "Public"
@image(`Set the Group Visibility`,../resources/img/setup/gitlab/hosting-gitlab-start-new-repo-4.png)

5. Navigate back to your project, then on the nav bar go to "Settings" -> "General". We should now have the option to set this repository to "Public":
@image(`Set the Project Visibility`,../resources/img/setup/gitlab/hosting-gitlab-start-new-repo-5.png)

6. Update this dropdown, then scroll down to the "Save changes" button:
@image(`Save Changes`,../resources/img/setup/gitlab/hosting-gitlab-start-new-repo-6.png)

## Check In

@check_in

# LiaScript

## What is LiaScript?

LiaScript was designed and developed by André Dietrich as a way to help people create and share
open source material online without requiring a significant preexisting technological knowledge
base.
To that end, LiaScript is a "flavor" of Markdown - for the most part, the process for writing a
workshop or interactive textbook is very similar to writing a README file. A user really only needs
to understand a few basic formatting concepts in order to create and share professional looking and
interactive content.

This also means that the source code for a given LiaScript course is decidedly human readable. If
someone would like to collaborate or add content to a course, the raw data that generates the
content is simple to follow and edit.

LiaScript is also meant to be accessible. In our documents we can add text which should
be spoken out load via the browser's speech engine. We can also provide alternate translations of
a document to allow users to select their preferred language.

There are of course additional bells and whistles that can be included to extend the functionality
of a given LiaScript document - we can add interactive code blocks, animations, plots, real time
surveys and more.

## How does LiaScript work?

The content of the course can be hosted anywhere, as long as the URL is publicly accessible to
the LiaScript engine. This can be a self hosted website, a public data repository, or a
GitHub/GitLab repository. This link can be provided to a hosted version of the LiaScript site
generator, which creates a link that anyone can use to access your content.

A freely available version of this can be found here: https://liascript.github.io/

@image(`The LiaScript Loader`,../resources/img/liascript-course-load-img.png)

            {{1-2}}
<section>
Navigate to https://liascript.github.io/ in your browser and try pasting the following url into the
input box labelled "Enter your course url..."

```
https://git.rwth-aachen.de/dl/workshops/creating-a-workshop-with-liascript/-/raw/main/lia/test.md
```

You should see something that looks like the following:

@image(`The LiaScript Loader`,../resources/img/lia-check/test-file-loaded.PNG)

            {{2}}
<section>
You'll note that there is no additional setup or downloads, and the content is immediately available
at the generated link. Once your content is stored online, the only thing you need to provide in order to share it is
the url at the top of the page.

</section>

# Sharing Content

## Hosting Content

One of the benefits of a format like LiaScript is that all of the course content can be hosted in
a git repository, like GitLab or GitHub. As long as the repository is publicly accessible, you
can provide the url of the raw file to the LiaScript site generator.

One of the benefits of a git repository like GitHub or GitLab is that we can directly link to a
specific file in that repository, which we can provide to the LiaScript renderer. This file, for
example, can be viewed directly at this url:

```
https://git.rwth-aachen.de/dl/workshops/creating-a-workshop-with-liascript/-/raw/main/lia/workshop.md
```

Let's try it now:

### Create an Example LiaScript File

We're going to use a very simple LiaScript file for this. Create a new document in your repository
and add a file called "my_workshop.md". Then, edit that file and paste the following markdown code
into it:

```
# Example LiaScript File

This is an example LiaScript File created by {your name here}.
```

Then commit the changes to the repository.

### Viewing the Site

The only piece of information we need now is the url to the raw markdown file. You can get that
here:

@image(`Finding the raw markdown file on GitLab`,../resources/img/hosting/view-raw.png)

Then, we head to https://liascript.github.io/ and paste our url into the input box:

@image(`Input the url into the LiaScript link generator`,../resources/img/hosting/input-link.png)

You will be automatically forwarded to a new page, which should be showing you the content we
just committed to the repository:

@image(`The page is now viewable`,../resources/img/hosting/view-user-file.png)

The URL at the top of this page is something you can send anyone you would like to show this
content.

Try going back to your repository and adding some additional text to the file. Once you commit the
updated file, refresh this page and we should see our changes.

@image(`Editing the markdown file in the repository`,../resources/img/hosting/edit-file.png)
@image(`The updated page`,../resources/img/hosting/view-user-file-2.png)


> @important The LiaScript Renderer will cache your files, so you may find that when you update
> your content, you have to "force refresh" the page with `Ctrl-F5` (`CMD-R` on Mac).

Note that this also means that we can get urls for branches, so if you are working collaboratively
on a document with multiple partners, you can provide links to raw files as they exist on
different branches, which might facilitate working with merge requests etc.

## Editors and IDEs

That process may work, but it's hardly an efficient workflow. Luckily there are a couple of tools
that can make the process easier. These can either be installed locally, or used online.

Online Editors
---

There is an online editor in development that can be found here: https://liascript.github.io/LiveEditor/.

This is a brand new feature and still in development, but should allow you to quickly try out
different LiaScript features without having to commit to a repository until you are satisfied with
how your document looks and feels.

IDE Extensions
---

There are extensions available for Atom and VSCode that will allow you to easily edit and locally
view LiaScript files:

[VS Code: LiaScript-Preview](https://marketplace.visualstudio.com/items?itemName=LiaScript.liascript-preview)
[Atom with LiaScript](https://aizac.herokuapp.com/install-atom-with-liascript/) (Atom is no longer in active development)

With the VS Code Extension installed, we can hit the shortcut `Alt-L` and a new browser tab appears
with a copy of the document we're currently working on as it would appear hosted online. This lets
us quickly locally try out different things in our document, without having to make commits to the
repository.

## Check In

@check_in

# LiaScript Flavored Markdown

For the most part, if you are familiar with Markdown, LiaScript will come fairly naturally, since
many of the concepts are identical. We're going to quickly go over some of the basic formatting
options that we can use, most of which follow the same or nearly the same syntax as markdown.

If you would like to reference the markdown syntax, you can find a good reference here:
https://www.markdownguide.org/basic-syntax/

At this point we are going to start editing the "my_workshop.md" file we created earlier. If you
don't have that file, you can create a new one in your repository. We will be adding content to
this file as we go along.

Remember that you can either use the VS Code extension to preview your changes, or you can commit
the changes to the repository and view them online.

## Headings & Sections

We can specify particular headings with the `#` character, as we do in markdown. Each additional
hash is used to determine the hierarchy of your content:

```
# H1
Content!
## H2
More Content!
### H3
#### H4
##### H5
```

which results in something that looks like this:

@image(`A document with headings`,../resources/img/lia-markdown/headings.png)

Let's add some sections to our markdown file:

```
# Example LiaScript File

This is an example LiaScript File created by {your name here}.

# Main Content

Here is where we'll talk about the main topic

## Sub Point A

There might be several..

## Sub Point B

..sections of things..

## Sub Point C

.. we might want to talk about.

### Side Note

It all ends up in a hierarchy

# Second Piece of Content

And here's more.
```

The idea here is that we can use the heading tags to create a structure for our document, which
will allow users to skip around to the sections they are interested in. This can result in very
short sections however, so you can also use...

## Text Formatting

Likewise, we can also set text to **bold**, *italic*, and ~strikethrough~ the same as we do in
markdown:

```
**bold**, *italic*, and ~strikethrough~
```

> **bold**, *italic*, and ~strikethrough~

LiaScript expands on these with a few extra formats that don't appear in regular markdown:

```
~~underline~~, ~~~strike and underline~~~, and ^superscript^
```

> ~~underline~~, ~~~strike and underline~~~, and ^superscript^

Let's update our file with some formatting:

```
# Example LiaScript File

This is an example LiaScript File created by {your name here}.

# Main Content

Here is where we'll talk about the main topic

## Sub Point A

There might be several..

## Sub Point B

..*sections* of things..

## Sub Point C

.. we might want to talk about.

### Side Note

It all ends up in a **hierarchy**

# ^Second^ Piece of Content

And here's more. Note that the text formatting even works in the headers.
```

## Links

We can add clickable links to our documents like this:

```
[This is the text that will appear](https://a_site_to_link_to.com)
```

So for example:

```
[LiaScript](https://liascript.github.io/)
```

Renders like this: [LiaScript](https://liascript.github.io/)

Adding this to our document:

```
# Example LiaScript File

This is an example LiaScript File created by {your name here}.

# Main Content

Here is where we'll talk about the main topic.

Here's a link to the LiaScript site: [LiaScript](https://liascript.github.io/)

## Sub Point A

There might be several..

## Sub Point B

..*sections* of things..

## Sub Point C

.. we might want to talk about.

### Side Note

It all ends up in a **heierarchy**

# ^Second^ Piece of Content

And here's more. Note that the text formatting even works in the headers.
```

## Lists

Unordered lists are identical to regular markdown:

```
- First level item
  - second level item
  - another item
    - third level item
```

> - First level item
>
>   - second level item
>   - another item
>
>     - third level item

Like markdown, we can use either `-`, `*`, or `+` to denote a list item.

The only gotcha with LiaScript lists is that there needs to be a space before and after the list
changes levels, or it is interpreted as being part of the same text:

```
This is some text that appears normally in a document
- Here is a list that doesn't have a space
- another list item
```

> This is some text that appears normally in a document
> - Here is a list that doesn't have a space
> - another list item

```
This is another text object that appears normally

- Another list, but this time with a space before

  - Sub item 1
  - Sub item 2

    - Third level, also with a space
```
> This is another text object that appears normally
>
> - Another list, but this time with a space before
>
>   - Sub item 1
>   - Sub item 2
>
>     - Third level, also with a space

Ordered lists work the same way, but with numbers followed by a period instead:

```
This is some text

1. My first ordered list item

  - sub item A
  - sub item B

2. My second ordered list item
```

> This is some text
>
> 1. My first ordered list item
>
>   - sub item A
>   - sub item B
>
> 2. My second ordered list item

Note that we can mix together ordered and unordered lists

We'll add a list to our document:

```
# Example LiaScript File

This is an example LiaScript File created by {your name here}.

# Main Content

Here is where we'll talk about the main topic.

Here's a link to the LiaScript site: [LiaScript](https://liascript.github.io/)

## Sub Point A

There might be several..

## Sub Point B

..*sections* of things..

1. With
2. Multiple
3. Points

  - To discuss
  - at length

4. In lists

## Sub Point C

.. we might want to talk about.

### Side Note

It all ends up in a **heierarchy**

# ^Second^ Piece of Content

And here's more. Note that the text formatting even works in the headers.
```

## Blockquotes

Blockquotes are also identical to markdown:

```
> This is a blockquote
>
> - with a list
> - and some more text
```

> This is a blockquote
>
> - with a list
> - and some more text

Just like in markdown, we can also nest blockquotes:

```
> This is a blockquote
>
> > This is a nested blockquote
> >
> > - with a list
> > - and some more text
```

> This is a blockquote
>
> > This is a nested blockquote
> >
> > - with a list
> > - and some more text

Let's add a quote to the top of our document:

```
# Example LiaScript File

This is an example LiaScript File created by {your name here}.

# Main Content

Start things off with a quote:

> May the force be with you
>
> -- Jean-Luc Picard (LOTR: The Two Towers)

Here is where we'll talk about the main topic.

Here's a link to the LiaScript site: [LiaScript](https://liascript.github.io/)

## Sub Point A

There might be several..

## Sub Point B

..*sections* of things..

1. With
2. Multiple
3. Points

  - To discuss
  - at length

4. In lists

## Sub Point C

.. we might want to talk about.

### Side Note

It all ends up in a **hierarchy**

# ^Second^ Piece of Content

And here's more. Note that the text formatting even works in the headers.
```

## Images

Images are broadly similar to markdown. We still use the `[title](src)` notation, but we also add
an `!` in front of the line like this:

```
![This is an example image](../resources/img/lia-markdown/wally.jpeg)
```

Which comes out like this:

![This is an example image](../resources/img/lia-markdown/wally.jpeg)

**Adding an image title**

You can see I'm adding some text after the image, which is the "title" of the image. This is used
to provide a annotation for the image, and is added within the parentheses after the image source
link and separated by a space:

```
![This is an example image](../resources/img/gosling-7938445_1280.jpg "(Image From https://pixabay.com/photos/gosling-chick-bird-baby-goose-7938445/)")
```

![This is an example image](../resources/img/gosling-7938445_1280.jpg "(Image From https://pixabay.com/photos/gosling-chick-bird-baby-goose-7938445/)")

**Hosting Images**

Note that this is an image which is contained in my repository:

![The Example image in my repository](../resources/img/lia-markdown/image_in_repo.png "The Example image in my repository")

The source of the image is the relative path from the current document to that image, so with
`../resources/img/placeholder.png`, I say "go up one level, then into the resources folder, then
the img folder, and use the placeholder.png file.

Images can also be included one after another - as long as they are part of the same paragraph,
this will generate an image carousel automatically:

```
![This is an example image](../resources/img/placeholder.png)
![This is a second example image](../resources/img/placeholder.png)
![This is a third example image](../resources/img/placeholder.png)
![This is a fourth example image](../resources/img/placeholder.png)
![This is a fifth example image](../resources/img/placeholder.png)
![This is a sixth example image](../resources/img/placeholder.png)
```

![This is an example image](../resources/img/placeholder.png)
![This is a second example image](../resources/img/placeholder.png)
![This is a third example image](../resources/img/placeholder.png)
![This is a fourth example image](../resources/img/placeholder.png)
![This is a fifth example image](../resources/img/placeholder.png)
![This is a sixth example image](../resources/img/placeholder.png)

## Check In

@check_in

# Quizzes and Checking for Understanding

## Basic Question Syntax

### Multiple Choice

We can add a multiple choice question by adding a line that starts with `[[ ]]` and then adding
the possible answers on the following lines. The correct answer should be indicated by adding an
`x` in the square brackets:

```
This is a question that has more than one answer

[[x]] Correct answer
[[ ]] Incorrect answer
[[ ]] Not this one
[[x]] Also this one
[[ ]] Nope
```

This is a question that has more than one answer

> [[x]] Correct answer
> [[ ]] Incorrect answer
> [[ ]] Not this one
> [[x]] Also this one
> [[ ]] Nope

Note here that the user can select multiple answers, and until the user selects both correct
answers, the question is marked as incorrect.

### Single Choice

Single choice questions are created nearly identically to multiple choice questions, but the user
can only select one answer:

```
This is a question that has only one answer

[(x)] Correct answer
[( )] Incorrect answer
[( )] Not this one
[( )] Nope
```

This is a question that has only one answer

> [(x)] Correct answer
> [( )] Incorrect answer
> [( )] Not this one
> [( )] Nope

Here, the quiz is rendered using radio buttons, so the user can only select one answer.

### Short Answer

Short answer questions are creating by a double indentation, and the correct answer is indicated
in a double square bracket:

```
This is a question that has a short answer

        [[foo]]
```

> This is a question that has a short answer
>
>         [[foo]]

### Hints

We can add hints to a given question by adding a line that starts with `[[?]]`:

```
This is a question that has a hint

    [[boo]]
    [[?]] A ghost might say this
    [[?]] A response to a poor performance
```

> This is a question that has a hint
>
>    [[boo]]
>    [[?]] A ghost might say this
>    [[?]] A response to a poor performance


The user can click on the <img src="../resources/img/placeholder.png" style="width: 30px"> icon on the right side of the question to reveal the hint.

## Classrooms

Although these quiz tools are readily usable in a sort of "living textbook", if we have multiple
users all interacting with the same document at once we can make use of the "classrooms" feature.

@image(`Starting a classroom`,../resources/img/quizzes/start_classroom.png)

Clicking the "Classroom" button brings us to a screen where we can select a backend:

@image(`Starting a classroom`,../resources/img/quizzes/select_db.png)

When we're connected to a classroom you can see the number of users connected to the same instance:

@image(`Starting a classroom`,../resources/img/quizzes/classroom_running.png)

## Check In

@check_in

# Metadata / Macros

## Document Metadata

We can provide at the top of the document some quick information about the authors and this
document like so:

```
<!--
@author: Jonathan Hartman
@date: March 19th, 2024
@email: hartman@itc.rwth-aachen.de
@repository: https://git.rwth-aachen.de/dl/workshops/creating-a-workshop-with-liascript
version: 0.1.0
-->
```

You can see this when you click on the little information icon on the far upper right of the page:

@image(`Metadata as it appears on the page`,../resources/img/macros/metadata.png)

The `@` sign denotes a macro - small bits of code that we can include in the document to avoid
repeating the same bits of code over and over. Some macros, like `@author` and `@email` are built
in, but we can also define our own

## Macros

Surprise! I've been using macros all along in this document! Macros go at the top of the document,
just below the Metadata we talked about in the last section.

### Single Line Macros

Sometimes it's just handy to have a quick little piece of code that I can throw in when I need it,
like this:

```
@important: <b style="color: red"> Important! </b>
```

Whenever I put the token `@important` in my document, I get this: @important

### Multi Line Macros

Here's a slightly more interesting example: I wanted to have the little "Check In" quizzes at
various points, but I kept changing my mind about what should be in them and how they should look.
Instead of writing the same code multiple times and having to search through the document to find
them all, I just wrote a macro:

```
@check_in
I'll pause here for a moment. Please click the radio button and hit "Submit" when you're ready.

  [(1)] I'm ready to move on!

---

*And if you have time...*

Do you have any questions or comments based on what we just covered?

  [[___]]

How is my pace so far?

  [(Too Slow)(Ok)(Too Fast)]
  [                        ] Pace

@end
```

@important Note that unlike the single line macro, I have to specify when the macro ends with the
`@end` token.

### Paramaterized Macros

We can also add parameters to macros:

The parameters are identified with `@`, starting at `@0`:

```
@fancy_text: <@2 style="color: @1"> @0 </@2>
```

We can invoke this in our document like this:

```
@fancy_text("This is some Fancy Text!", blue, b)
```

The values are directly interpolated into the macro, so this macro resolves to
`<b style="color: blue"> "This is some Fancy Text!" </b>`

Which renders like this: @fancy_text("This is some Fancy Text!", blue, b)

```
@fancy_text("Here's some different text", green, i)
```

@fancy_text("Here's some different text", green, i)

## Check In

@check_in

# Additional Content

Here's a list of some of the other topics that I can speak to if there is any interest:

- Custom Styling with CSS
- Interactive Code Blocks
- Extensions (e.g. Python via pyodide)

Below here is just some reference code related to these topics

## CSS

`link: ../resources/style/custom.css`

```
.custom-image img {
    width: auto;
    margin-left: auto;
    margin-right: auto;
    display: block;
    border: 1px solid darkgrey;
    box-shadow: 5px 5px 5px;
}
```

```
@image
<div class="custom-image">
![@0](@1 "@0")
</div>
@end
```

## Interactive Code

```
``` javascript
var i=0;
var j=0;
var result = 0;

for(i = 0; i<10; i++) {
    for(j = 0; j<i; j++) {
        result += j;
        console.log("(", i, ",", j, ") result", result)
    }
}
// the last statement defines the return statement
result;
```
<script>@input</script>
```

## Pyodide

https://liascript.github.io/course/?https://raw.githubusercontent.com/LiaTemplates/Pyodide/master/README.md

https://github.com/LiaTemplates/Pyodide